package com.bjj.service.utils;

import com.bjj.service.project.common.inf.INamingMap;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.remoting.RemoteLookupFailureException;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ProxyRepository implements ApplicationContextAware, BeanFactoryAware {
    private static ApplicationContext applicationContext;
    private static String proxyRepositoryBeanId = "proxyRepository";
    private String rmiServerIP = "127.0.0.1";
    private String rmiServerPort = "9876";
    private String rmiServerName = "namingSrv";//default
    private Map<String, RmiProxyFactoryBean> rmiBean = Collections.synchronizedMap(new LinkedHashMap<String, RmiProxyFactoryBean>());
    private boolean serviceInit = false;

    private Map<String, String> namingMap;
    private boolean namingMapInit = false;
    private BeanFactory beanFactory;

    private ProxyRepository() {
        super();
    }


    public static ProxyRepository getInstance() {
        return (ProxyRepository) applicationContext.getBean(proxyRepositoryBeanId);
    }

    public String getProxyRepositoryBeanId() {
        return proxyRepositoryBeanId;
    }

    public void setProxyRepositoryBeanId(String proxyRepositoryBeanId) {
        ProxyRepository.proxyRepositoryBeanId = proxyRepositoryBeanId;
    }

    private void initNamingMap() {
        namingMapInit = false;

        try {
            RmiProxyFactoryBean baseRmi = createRmiBean(rmiServerName, INamingMap.class);
            INamingMap instance = (INamingMap) baseRmi.getObject();
            namingMap = instance.getNamingMap();
            namingMapInit = true;
        } catch (RemoteLookupFailureException e1) {
            System.err.println("-------------------【ERROR】RMI init NameService(no " + rmiServerName + ") by:" + e1.getMessage() + "-------------------------");
        } catch (RemoteConnectFailureException e1) {
            System.err.println("-------------------【ERROR】RMI init NameService(connecting " + rmiServerName + ") by:" + e1.getMessage() + "-------------------------");
        }
    }

    private void connectAllService() {
        serviceInit = false;


        if (namingMapInit) {
            Iterator<Entry<String, String>> iterator = namingMap.entrySet().iterator();
            int local = 0;
            while (iterator.hasNext()) {
                Entry<String, String> next = iterator.next();

                String key = next.getKey();
                String value = next.getValue();
                try {
                    Class interfc = Class.forName(value);
                    RmiProxyFactoryBean createRmiBean = createRmiBean(key, interfc);
                    rmiBean.put(key, createRmiBean);

                    if (beanFactory != null && beanFactory instanceof DefaultListableBeanFactory) {
                        try {
                            DefaultListableBeanFactory dlBeanFactory = (DefaultListableBeanFactory) beanFactory;
                            dlBeanFactory.registerSingleton(key, createRmiBean);
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    }

                    System.out.println("\t[√]:" + key + "@" + value);
                    local++;
                } catch (ClassNotFoundException e) {
                    System.err.println("\t[X]:" + key + "@" + value);
                }
            }
            serviceInit = true;
            System.out.println("\t【INFO】RMI all services size:" + namingMap.size() + " local:" + local + "");
        } else {
            System.err.println("\t【ERROR】RMI all services not initialled(no namingMap)");
        }
    }

    private void init() {
        System.out.println("-------------------RMI Init Begin-------------------------");

        System.out.println("Please check your host:port servicename as follows!");
        System.out.println("=> " + rmiServerIP + ":" + rmiServerPort + " " + rmiServerName);
        System.out.println("The bean Id of proxyRepository in the Spring is " + proxyRepositoryBeanId + "!");
        //load命名服务
        initNamingMap();

        //连接所有服务
        connectAllService();

        System.out.println("-------------------RMI Init End-------------------------");
    }

    private void tryInit() {
        if (!namingMapInit) {
            System.out.println("-------------------【WARN】try load namingMap-------------------------");

            //load命名服务
            initNamingMap();
        }

        if (!serviceInit) {
            System.out.println("-------------------【WARN】try connect all services-------------------------");

            //连接所有服务
            connectAllService();
        }

    }

    private RmiProxyFactoryBean createRmiBean(String serverName, Class inf) throws RemoteLookupFailureException {
        RmiProxyFactoryBean rmiProxyFactoryBean = new RmiProxyFactoryBean();
        rmiProxyFactoryBean.setRefreshStubOnConnectFailure(true);
        String rmiUrl = "rmi://" + rmiServerIP + ":" + rmiServerPort + "/" + serverName;
        rmiProxyFactoryBean.setServiceInterface(inf);
        rmiProxyFactoryBean.setServiceUrl(rmiUrl);
        rmiProxyFactoryBean.afterPropertiesSet();
        return rmiProxyFactoryBean;
    }

    @Override
    public final void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        applicationContext = applicationContext;
    }

    public String getRmiServerIP() {
        return rmiServerIP;
    }

    public void setRmiServerIP(String rmiServerIP) {
        this.rmiServerIP = rmiServerIP;
    }

    public String getRmiServerPort() {
        return rmiServerPort;
    }

    public void setRmiServerPort(String rmiServerPort) {
        this.rmiServerPort = rmiServerPort;
    }

    public Map<String, String> getNamingMap() {
        return namingMap;
    }

    public RmiProxyFactoryBean getProxy(String name) {
        tryInit();
        return rmiBean.get(name);
    }

    public Map<String, RmiProxyFactoryBean> getRmiBean() {
        return rmiBean;
    }

    public String getRmiServerName() {
        return rmiServerName;
    }

    public void setRmiServerName(String rmiServerName) {
        this.rmiServerName = rmiServerName;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {

        this.beanFactory = beanFactory;
    }
}
