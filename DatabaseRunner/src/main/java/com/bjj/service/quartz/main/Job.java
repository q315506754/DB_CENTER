package com.bjj.service.quartz.main;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 14:11
 */
public interface Job {
    void execute();
}
