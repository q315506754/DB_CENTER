package com.bjj.service.project.webfront.db;

import com.bjj.service.model.webfront.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface UserRepository extends MongoRepository<User, ObjectId> {
    @Query("{'userName':?0,'passWord':?1}")
    public User findUserByNameAndPwd(String userName, String passWord);

    @Query(value = "{'userName':?0}", fields = "{mobile:1}")
    public User findMobileByName(String userName);

    @Query(value = "{'userName':?0}")
    public User findUserByName(String userName);

    @Query(value = "{'mobile':?0}")
    public User findUserByMobile(String mobile);
    
    @Query(value = "{'_id':?0}")
    public User findUserById(ObjectId adminId);

    @Override
    public <S extends User> S save(S arg0);

}
