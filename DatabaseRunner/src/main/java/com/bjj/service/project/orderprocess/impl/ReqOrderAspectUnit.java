package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.model.webfront.ReqOrder;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:29
 */
@Component
public class ReqOrderAspectUnit extends ReqOrderAspect {

    public ReqOrderAspectUnit() {
        super("unit");
    }

    @Override
    public JSONObject beforeChange(Object oldVal, Object newValue, ReqOrder one, Update update, JSONObject upparams) throws OrderPImpl.UpdateInterruptException {
        try {
            JSONArray unit = JSONArray.fromObject(newValue);
            double totalPrice = 0d;
            for (Object o : unit) {
                try {
                    JSONObject json = (JSONObject) o;
                    totalPrice = totalPrice + json.getDouble("totalPrice");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            update.set("totalPrice", totalPrice);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject ret = new JSONObject();
        return ret;
    }
}
