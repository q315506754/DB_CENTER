package com.bjj.service.project.common.impl;

import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.project.common.inf.INamingMap;
import com.bjj.tools.PropertiesUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanCurrentlyInCreationException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.remoting.rmi.RmiServiceExporter;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author Jiangli
 *         <p/>
 *         CreateTime 2013-8-7 下午4:13:22
 */
//@Component("rmi_bean_naming")
public class NamingMapImplBeanForAnnotation implements INamingMap, ApplicationContextAware, BeanFactoryAware {

    private static final Log logger = LogFactory.getLog("Runner");
    private final static Map<String, String> map = new HashMap<String, String>();

    static {
        //init();
    }

    private ApplicationContext applicationContext;
    private BeanFactory beanFactory;

    private void init() {
        try {
//            ApplicationContext applicationContext = SpringUtil.getApplicationContext();
            String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
            Field serviceName = RmiServiceExporter.class.getDeclaredField("serviceName");
            serviceName.setAccessible(true);
            //System.out.println("[" + DateUtil.getCurrentDate_YMDHms() + "] Init RMI Services Start ...");
            logger.info("\tInitializing Naming Services  Start ...");

            Map<String, List<String>> checkDuplicateInterfaceMap = new LinkedHashMap<String, List<String>>();

            int i = 1;
            int errCount = 0;
            for (String bean : beanDefinitionNames) {
                try {
                    if (bean.equals("namingServiceExporter") || bean.equals("namingService")) {
                        continue;
                    }
                    Object tbean = applicationContext.getBean(bean);
                    if (!tbean.getClass().isAnnotationPresent(RMIServiceExporterAnno.class)) {
                        continue;
                    }

                    RMIServiceExporterAnno annotation = tbean.getClass().getAnnotation(RMIServiceExporterAnno.class);
                    Class<?> serviceInterface = annotation.serviceInterface();
                    if (!serviceInterface.isAssignableFrom(tbean.getClass())) {
                        logger.error("\t\t" + (i + errCount++) + ". 【ERROR】rmiImplBean:" + tbean.getClass().getCanonicalName() + " does not implement interface:" + tbean.getClass().getCanonicalName() + " configured in @RMIServiceExporter(" + annotation.serviceInterface().getCanonicalName() + ")");
                        continue;
                    }

                    RmiServiceExporter serviceExporter = new RmiServiceExporter();
                    String beanName = "rmi_service_exporter_bean_" + bean;
                    String rmiServiceName = PropertiesUtil.readStringValue("rmi.service.prefix") + bean;
                    serviceExporter.setServiceName(rmiServiceName);
                    serviceExporter.setService(tbean);
                    serviceExporter.setServiceInterface(serviceInterface);
                    serviceExporter.setRegistryPort(Integer.parseInt(PropertiesUtil.readStringValue("rmi.server.port")));
                    serviceExporter.prepare();

                    if (beanFactory != null && beanFactory instanceof DefaultListableBeanFactory) {
                        try {
                            DefaultListableBeanFactory dlBeanFactory = (DefaultListableBeanFactory) beanFactory;
                            dlBeanFactory.registerSingleton(beanName, serviceExporter);
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    }

                    String serverName = serviceName.get(serviceExporter).toString();
                    String cls = serviceExporter.getServiceInterface().getCanonicalName();
                    map.put(serverName, cls);
                    int totalOrdin = i + errCount;
                    logger.info("\t\t" + totalOrdin + ". " + serverName + "==>" + cls);
                    i++;

                    List<String> registeredSrves = checkDuplicateInterfaceMap.get(cls);
                    if (registeredSrves == null) {
                        registeredSrves = new LinkedList<String>();
                        checkDuplicateInterfaceMap.put(cls, registeredSrves);
                    }
                    registeredSrves.add(totalOrdin + ". " + serverName);

                } catch (Exception e) {
                    logger.error("Naming Services Init bean error:" + bean, e);
                    e.printStackTrace();
                }
            }

            logger.info("\t" + "The total count of registered services is " + (i - 1) + " , not registered " + errCount);

            //check for MultiImplement
            for (String infName : checkDuplicateInterfaceMap.keySet()) {
                List<String> list = checkDuplicateInterfaceMap.get(infName);
                if (list.size() > 1) {
                    logger.error("\t" + "ExtraInfo:【WARN】");
                    logger.error("\t" + "These interface has multiple implements, the @Autowired should not be used, using @Resource(name = \"" + PropertiesUtil.readStringValue("rmi.service.prefix") + "xxxx\") instead.");

                    logger.error("\t\t" + infName);
                    for (String srvBeanName : list) {
                        logger.error("\t\t\t" + srvBeanName);
                    }
                }
            }

            if (errCount == 0) {
                logger.info("\t" + "Result:【Succeed】 listened port:" + PropertiesUtil.readStringValue("rmi.server.port"));
            } else {
                logger.error("\t" + "Result:【Failed】You have " + errCount + " error remained, please shut the runner and check the information above and then fix it!");
            }
        } catch (BeanCurrentlyInCreationException e) {
            logger.error("Naming Services Init BeanCurrentlyInCreationException ===> ", e);
        } catch (BeanCreationException e) {
            logger.error("Naming Services Init BeanCreationException ===> ", e);
        } catch (Exception e) {
            logger.error("Naming Services Init Exception ===> ", e);
        }

        //System.out.println("[" + DateUtil.getCurrentDate_YMDHms() + "] Init RMI Services End ...");

        logger.info("\tInitializing Naming Services  End ...");
    }

    @Override
    public Map<String, String> getNamingMap() {
        return map;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {

        this.beanFactory = beanFactory;
    }
}
