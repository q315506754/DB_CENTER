package com.bjj.service.project.webfront.db;

import com.bjj.service.model.webfront.Area;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface AreaRepository extends MongoRepository<Area, ObjectId> {
    @Query("{'cityId':?0}")
    public List<Area> findAreasByCityId(ObjectId cityId);
}
