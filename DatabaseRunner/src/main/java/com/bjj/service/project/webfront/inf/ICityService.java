package com.bjj.service.project.webfront.inf;


import com.bjj.service.project.webfront.domain.City;

import java.util.List;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/6 0006 14:26
 */
public interface ICityService {
    List<City> getAllCitys();

    List<City> getCitysLikeName(String name);
}
