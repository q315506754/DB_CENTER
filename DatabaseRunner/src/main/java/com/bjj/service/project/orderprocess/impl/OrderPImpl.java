package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.OrderStatus;
import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.domain.Msg;
import com.bjj.service.exception.MsgException;
import com.bjj.service.model.orderprocess.DZFOrderIds;
import com.bjj.service.model.webfront.*;
import com.bjj.service.project.orderprocess.domain.QueryOrderParams;
import com.bjj.service.project.orderprocess.inf.IOrderPInf;
import com.bjj.service.quartz.jobs.CancelDZYOrders;
import com.bjj.service.utils.CommonUtil;
import com.bjj.service.utils.DateUtils;
import com.bjj.tools.LogUtil;
import com.mongodb.WriteResult;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/25 0025 12:22
 */
@Component
@RMIServiceExporterAnno(serviceInterface = IOrderPInf.class)
public class OrderPImpl implements IOrderPInf {
    private LogUtil log = LogUtil.getInstance("orderProcess");

    @Autowired
    private MongoOperations mongoOperationNew;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private List<ReqOrderAspect> aspects;

    @Autowired
    private CancelDZYOrders cancelJob;

    @Override
    public JSONObject queryOrder(QueryOrderParams params) {
        JSONObject ret = new JSONObject();
        Query query = new Query();
        query.skip(params.getSkip());
        query.limit(params.getPageSize());
        query.with(new Sort(new Sort.Order(Sort.Direction.DESC, "createTime")));

        /* common query*/
        regexQueryField(query, params.getOrderNum(), params.getOrderNumReg(), "orderId");
        regexQueryField(query, params.getPropName(), params.getPropNameReg(), "hotelName");
        regexQueryField(query, params.getContactName(), params.getContactNameReg(), "contactName");
        regexQueryField(query, params.getContactPhone(), params.getContactPhoneReg(), "contactPhone");

        /* status*/
        if (CommonUtil.isStringNotNull(params.getStatus())) {
            String[] split = params.getStatus().split(",");
            query.addCriteria(Criteria.where("status").in(split));
        }
        
        /* userId query*/
        if (CommonUtil.isStringNotNull(params.getUserId()) && params.getUserId().length() == 24) {
            query.addCriteria(Criteria.where("userId").is(new ObjectId(params.getUserId())));
        }

        /* date query*/
        Long orderDate = parseDate(params.getOrderDate());
        if (orderDate != null) {
            Criteria criteria = new Criteria();
            criteria.andOperator(Criteria.where("createTime").gte(orderDate), Criteria.where("createTime").lt(dateUtils.getNextDate(new Date(orderDate)).getTime()));
            query.addCriteria(criteria);
        }

        Long startDate = parseDate(params.getStartDate());
        if (startDate != null) {
            query.addCriteria(Criteria.where("startDate").is(startDate));
        }

        Long endDate = parseDate(params.getEndDate());
        if (endDate != null) {
            query.addCriteria(Criteria.where("endDate").is(endDate));
        }

        JSONArray data = new JSONArray();
        List<ReqOrder> reqOrders = mongoOperationNew.find(query, ReqOrder.class);
        long count = mongoOperationNew.count(query, ReqOrder.class);
        for (ReqOrder reqOrder : reqOrders) {
//            JSONObject one = JSONObject.fromObject(reqOrder);

            JSONObject obj = getJsonObjectFromModel(reqOrder);
            data.add(obj);
        }

        ret.put("data", data);
        ret.put("recordsFiltered", count);
        ret.put("recordsTotal", count);


        return ret;
    }

    private JSONObject getJsonObjectFromModel(ReqOrderLog reqOrderLog) {
        JSONObject obj = new JSONObject();
        obj.put("operTime", reqOrderLog.getOperTime());
        obj.put("operTimeStr", reqOrderLog.getOperTime() != null ? dateUtils.getDate_YYYY_MM_DD(reqOrderLog.getOperTime()) : "");
        obj.put("content", CommonUtil.nullToEmpty(reqOrderLog.getContent()));
        obj.put("operIp", CommonUtil.nullToEmpty(reqOrderLog.getOperIp()));
        obj.put("operAdminName", CommonUtil.nullToEmpty(reqOrderLog.getOperAdminName()));
        obj.put("operAdminMobile", CommonUtil.nullToEmpty(reqOrderLog.getOperAdminMobile()));

        return obj;
    }

    private JSONObject getJsonObjectFromModel(User user) {
        JSONObject obj = new JSONObject();
        obj.put("objId", CommonUtil.nullToEmpty(user.getId()));
        obj.put("userName", CommonUtil.nullToEmpty(user.getUserName()));
        obj.put("email", CommonUtil.nullToEmpty(user.getEmail()));
        obj.put("mobile", CommonUtil.nullToEmpty(user.getMobile()));
        obj.put("name", CommonUtil.nullToEmpty(user.getName()));
//        obj.put("passWord", CommonUtil.nullToEmpty(user.getPassWord()));

        return obj;
    }

    private JSONObject getJsonObjectFromModelExtra(ReqOrder reqOrder) {
        JSONObject obj = new JSONObject();

        String adminName = getLastOperRemarkAdminName(reqOrder.getOrderId());

        obj.put("lastOperRemarkAdminName", adminName);
        return obj;
    }

    private String getLastOperRemarkAdminName(String reqOrder) {
        String adminName = "";
        Query queryLog = new Query();
        queryLog.addCriteria(Criteria.where("orderId").is(reqOrder));
        queryLog.addCriteria(Criteria.where("operType").is("remark"));
        queryLog.with(new Sort(new Sort.Order(Sort.Direction.DESC, "operTime")));
        List<ReqOrderLog> reqOrderLogs = mongoOperationNew.find(queryLog, ReqOrderLog.class);
        if(reqOrderLogs.size() > 0) {
            adminName = CommonUtil.nullToEmpty(reqOrderLogs.get(0).getOperAdminName());
        }
        return adminName;
    }

    private JSONObject getJsonObjectFromModel(ReqOrder reqOrder) {
        JSONObject obj = new JSONObject();
        obj.put("orderNum", CommonUtil.nullToEmpty(reqOrder.getOrderId()));
        obj.put("objId", CommonUtil.nullToEmpty(reqOrder.getId()));
        obj.put("propName", CommonUtil.nullToEmpty(reqOrder.getHotelName()));
        obj.put("contactName", CommonUtil.nullToEmpty(reqOrder.getContactName()));
        obj.put("contactPhone", CommonUtil.nullToEmpty(reqOrder.getContactPhone()));
        obj.put("contactEmail", CommonUtil.nullToEmpty(reqOrder.getContactEmail()));
        obj.put("liveName", CommonUtil.nullToEmpty(reqOrder.getLiveName()));
        obj.put("okNum", CommonUtil.nullToEmpty(reqOrder.getOkNum()));
        obj.put("roomNumber", CommonUtil.nullToEmpty(reqOrder.getRoomNumber()));
        obj.put("roomTypeName", CommonUtil.nullToEmpty(reqOrder.getRoomTypeName()));
        obj.put("bedTypeName", CommonUtil.nullToEmpty(reqOrder.getBedTypeName()));
        obj.put("broadband", CommonUtil.nullToEmpty(reqOrder.getBroadband()));
        obj.put("breakfastNum", CommonUtil.nullToEmpty(reqOrder.getBreakfastNum()));
        obj.put("cancelRule", CommonUtil.nullToEmpty(reqOrder.getCancelRule()));
        obj.put("liveDay", CommonUtil.getDateDiff(reqOrder.getStartDate(), reqOrder.getEndDate()));

        obj.put("orderDate", reqOrder.getCreateTime() != null ? dateUtils.getDate_YYYYMMDD(reqOrder.getCreateTime()) : "");
        obj.put("orderTime", reqOrder.getCreateTime() != null ? dateUtils.getTime_HH_mm_ss(reqOrder.getCreateTime()) : "");
        obj.put("startDate", reqOrder.getStartDate() != null ? dateUtils.getDate_YYYYMMDD(reqOrder.getStartDate()) : "");
        obj.put("endDate", reqOrder.getEndDate() != null ? dateUtils.getDate_YYYYMMDD(reqOrder.getEndDate()) : "");
        OrderStatus orderStatus = OrderStatus.get(reqOrder.getStatus());
        obj.put("providerOrderNum", CommonUtil.nullToEmpty(reqOrder.getProviderOrderNum()));
        obj.put("remark", CommonUtil.nullToEmpty(reqOrder.getRemark()));

        long restDZFtime = 0l;
        DZFOrderIds restDZFtimeObj = CommonUtil.findOne(DZFOrderIds.class, "orderId", CommonUtil.nullToEmpty(reqOrder.getOrderId()));
        if (restDZFtimeObj != null) {
            long DZFts = restDZFtimeObj.getTime() == null ? 0l : restDZFtimeObj.getTime();
            restDZFtime = CancelDZYOrders.cancelTime + DZFts - System.currentTimeMillis();
            restDZFtime = restDZFtime < 0 ? 0l : restDZFtime;
        }
        obj.put("restDZFtime", restDZFtime);
        //待支付超时取消
        if (restDZFtime == 0 && orderStatus == cancelJob.E_TO_BE_CANCELLED_STATUS) {
            cancelJob.cancelOrders(Arrays.asList(reqOrder.getOrderId()));
            cancelJob.saveOrderLog(reqOrder.getOrderId());
            reqOrder.setStatus(cancelJob.E_CANCELLED_STATUS.getEnName());
            orderStatus = cancelJob.E_CANCELLED_STATUS;
        }
        obj.put("status", orderStatus.getEnName());
        obj.put("statusName", orderStatus.getCnName());


        List<UnitPrice> unit = reqOrder.getUnit();
        boolean noPrice = false;
        if (unit == null || unit.size() == 0) {
            noPrice = true;
            if (reqOrder.getStartDate() != null && reqOrder.getEndDate() != null && reqOrder.getStartDate() < reqOrder.getEndDate()) {
                unit = new LinkedList<UnitPrice>();
                for (long s = reqOrder.getStartDate(); s < reqOrder.getEndDate(); s += TimeUnit.DAYS.toMillis(1)) {
                    UnitPrice oneDayPrice = new UnitPrice();
                    oneDayPrice.setDate(s);
                    unit.add(oneDayPrice);
                }
            }
        }

        Double totalPrice = noPrice ? null : 0d;

        JSONArray unitArr = new JSONArray();
        if (unit != null) {
            for (UnitPrice unitPrice : unit) {
                JSONObject oneDayPrice = new JSONObject();
                oneDayPrice.put("date", unitPrice.getDate());
                oneDayPrice.put("inputPrice", CommonUtil.formatOrderPrice(unitPrice.getInputPrice()));
                oneDayPrice.put("totalPrice", CommonUtil.formatOrderPrice(unitPrice.getTotalPrice()));
                oneDayPrice.put("dateStr", unitPrice.getDate() != null ? dateUtils.getDate_YYYYMMDD(unitPrice.getDate()) : "");
                //每日价格里面的增值服务
                if(unitPrice.getExtraService()!=null&&unitPrice.getExtraService().size()>0){
                	JSONArray unitExtraServices = JSONArray.fromObject(unitPrice.getExtraService());
                	oneDayPrice.put("unitExtraServices", unitExtraServices);
                }
                unitArr.add(oneDayPrice);

                if (totalPrice != null && unitPrice.getTotalPrice() != null) {
                    totalPrice += unitPrice.getTotalPrice();
                }
            }
        }
        obj.put("unit", unitArr);


        JSONArray extraService = new JSONArray();
        if (reqOrder.getExtraService() != null) {
            for (ExtraService unitPrice : reqOrder.getExtraService()) {
                JSONObject oneDayPrice = new JSONObject();
                oneDayPrice.put("inputPrice", CommonUtil.formatOrderPrice(unitPrice.getInputPrice()));
                oneDayPrice.put("type", CommonUtil.nullToEmpty(unitPrice.getType()));
                oneDayPrice.put("remark", CommonUtil.nullToEmpty(unitPrice.getRemark()));
                extraService.add(oneDayPrice);

            }
        }
        obj.put("extraService", extraService);

        obj.put("totalPrice", CommonUtil.formatOrderPrice(totalPrice));


        ObjectId providerId = reqOrder.getProviderId();
        String providerName = null;
        if (providerId != null) {
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(providerId));
            Provider one = mongoOperationNew.findOne(query, Provider.class);
            if (one != null) {
                providerName = one.getName();
            }
        }
        obj.put("providerName", CommonUtil.nullToEmpty(providerName));
        obj.put("providerId", CommonUtil.nullToEmpty(providerId));



        return obj;
    }

    @Override
    public JSONObject orderLogs(String orderId) {
        if (CommonUtil.isStringNotNull(orderId)) {
            ReqOrder one = getReqOrderByStr(orderId);
            if (one != null) {
                String realOrderId = CommonUtil.nullToEmpty(one.getOrderId());

                Query queryLog = new Query();
                queryLog.addCriteria(Criteria.where("orderId").is(realOrderId));

                queryLog.with(new Sort(new Sort.Order(Sort.Direction.DESC, "operTime")));

                List<ReqOrderLog> reqOrderLogs = mongoOperationNew.find(queryLog, ReqOrderLog.class);
                JSONObject ret = new JSONObject();
                JSONArray data = new JSONArray();
                for (ReqOrderLog reqOrderLog : reqOrderLogs) {
//                JSONObject logOne = new JSONObject();
                    JSONObject logOne = getJsonObjectFromModel(reqOrderLog);
                    data.add(logOne);
                }
                ret.put("data", data);
                ret.put("orderId", realOrderId);
                return ret;
            }
        }

        return null;
    }

    @Override
    public JSONObject orderInfo(String orderId,String degree) {
        if (CommonUtil.isStringNotNull(orderId)) {
            ReqOrder one = getReqOrderByStr(orderId);
            if (one != null) {
                JSONObject ret = new JSONObject();
                ObjectId userId = one.getUserId();

                if (userId != null) {
                    User user = CommonUtil.findOneByObjectId(User.class, userId);
                    if(user != null){
                        JSONObject jsonObjectFromModel = getJsonObjectFromModel(user);
                        ret.put("userInfo", jsonObjectFromModel);
                    }
                }

                JSONObject jsonObjectFromModel = getJsonObjectFromModelExtra(one);
                ret.put("extraOrderInfo", jsonObjectFromModel);

                return ret;
            }
        }

        return null;
    }

    @Override
    public JSONObject orderDetail(String orderId) {
        if (CommonUtil.isStringNotNull(orderId)) {
            ReqOrder one = getReqOrderByStr(orderId);
            if (one != null) {
                JSONObject ret = getJsonObjectFromModel(one);

                JSONArray providers = new JSONArray();
                List<Provider> all = mongoOperationNew.findAll(Provider.class);
                for (Provider provider : all) {
                    JSONObject obj = JSONObject.fromObject(provider);
                    obj.put("id", provider.getId().toString());
                    providers.add(obj);
                }
                ret.put("providers", providers);
                return ret;
            }
        }

        return null;
    }

    private ReqOrder getReqOrderByStr(String orderId) {
        Query query = new Query();
        if (CommonUtil.isObjectId(orderId)) {
            query.addCriteria(Criteria.where("_id").is(new ObjectId(orderId)));
        } else {
            query.addCriteria(Criteria.where("orderId").is(orderId));
        }

        return mongoOperationNew.findOne(query, ReqOrder.class);
    }

    @Override
    public JSONObject updateOrder(JSONObject param) {
        Msg<JSONArray> ret = new Msg<JSONArray>();
        Object t_scene = param.get("_scene");

        try {
            Object o_pk = param.get("pk");
            Object o_update = param.get("update");
            ret.throwIfNullException(o_pk, 2, "没有传入订单_id");
            ret.throwIfNullException(o_update, 3, "没有传入更新参数");
            String pk = (String) o_pk;
            ret.throwIfStringNotObjectIdException(pk, 4, "订单_id不是ObjectId类型");
            Object t_remarkSplit = param.get("remarkSplit");
            Object t_remark = param.get("remark");//覆盖生成的操作内容
            String operAdminName = CommonUtil.nullToEmpty(param.get("operAdminName"));
            String operAdminMobile = CommonUtil.nullToEmpty(param.get("operAdminMobile"));
            String operIp = CommonUtil.nullToEmpty(param.get("operIp"));
            String remarkSplit = t_remarkSplit == null ? "<br/>" : t_remarkSplit.toString();
            JSONArray update = (JSONArray) o_update;
            if (update.size() == 0) {
                ret.throwException(0, "传入的更新字段为空,无需更新");
            }


            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(new ObjectId(pk)));

            ReqOrder one = mongoOperationNew.findOne(query, ReqOrder.class);
            ret.throwIfNullException(one, 5, "找不到该订单数据");

            Update updateDB = new Update();

            JSONArray data = new JSONArray();
            ReqOrderLog log = new ReqOrderLog();
            String operContent = convertUpdateParam(param,update, one, updateDB, data, remarkSplit,log);
            operContent = t_remark == null ? operContent : t_remark.toString();

            ret.setData(data);

            if (updateDB.getUpdateObject().keySet().size() == 0) {
                ret.throwException(0, "转换后的更新字段为空,无需更新");
            }

            WriteResult writeResult = mongoOperationNew.updateFirst(query, updateDB, ReqOrder.class);

            log.setContent(operContent);
            log.setOperTime(System.currentTimeMillis());
            log.setOrderId(one.getOrderId());
            log.setOperAdminName(operAdminName);
            log.setOperAdminMobile(operAdminMobile);
            log.setOperIp(operIp);

            mongoOperationNew.save(log);
        } catch (MsgException e) {

        } catch (Exception e) {
            log.logError(e.getMessage(), e);
            ret.error(1, "未知异常", e.getClass() + "\t" + e.getMessage());
        }

        JSONObject tRet = new JSONObject();
        tRet.put("code", ret.getCode());
        tRet.put("msg", ret.getMsg());
        tRet.put("results", ret.getData());
        tRet.put("_scene", t_scene);
        return tRet;
    }

    private String convertUpdateParam(JSONObject upparam,JSONArray update, ReqOrder one, Update updateDB, JSONArray data, String remarkSplit,ReqOrderLog log) {
        StringBuilder sb = new StringBuilder();
        int c = 0;
        for (Object o : update) {
            c++;

            JSONObject rsOne = new JSONObject();
            JSONObject u_one = (JSONObject) o;

            try {
                String name = u_one.getString("name");
                Object value = u_one.get("value");
                Object t_name_cn = u_one.get("name_cn");
                Object t_remark = u_one.get("remark");
                String name_cn = t_name_cn == null ? name : t_name_cn.toString();
                String eachRemark = t_remark == null ? name_cn + "值更新成了:" + value : t_remark.toString();
                eachRemark = eachRemark.replaceAll("<value>", CommonUtil.nullToEmpty(value));

                rsOne.put("key", name);
                rsOne.put("code", 0);
                rsOne.put("msg", "字段更新成功");

                try {
                    Class cls = one.getClass();
                    Field field = cls.getDeclaredField(name);
                    field.setAccessible(true);
                    Class<?> fieldType = field.getType();

                    value = convertValueType(value, fieldType);

                    Object property = field.get(one);
//                    System.out.println("property:"+property+" value:"+value);

                    //sure for not eq
                    if (property != null && property.equals(value)) {
                        UpdateInterruptException.throwEx(1, "字段值没有改变");
                    }

                    for (ReqOrderAspect aspect : aspects) {
                        if (aspect.getPropName().equals(name)) {
                            JSONObject retData = aspect.beforeChange(property, value, one, updateDB, upparam);
                            rsOne.put("data", retData);

                            aspect.modifyOrderLog(log);
                        }
                    }
                } catch (UpdateInterruptException e) {
                    rsOne.put("code", e.code);
                    rsOne.put("msg", e.msg);
                    data.add(rsOne);
                    continue;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                updateDB.set(name, value);

//                remark
                sb.append(eachRemark);
                if (c != update.size()) {
                    sb.append(remarkSplit);
                }
            } catch (Exception e) {
                rsOne.put("key", u_one.get("name"));
                rsOne.put("code", -100);
                rsOne.put("msg", "未知异常" + e.getMessage());
            }

            data.add(rsOne);
        }

        return sb.toString();
    }

    public static class UpdateInterruptException extends RuntimeException {
        int code;
        String msg;

        public static void throwEx(int code, String msg) throws UpdateInterruptException {
            throw new UpdateInterruptException(code, msg);
        }

        public UpdateInterruptException(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

    private Object convertValueType(Object orgvalue, Class<?> fieldType) throws UpdateInterruptException {
        Object value = orgvalue;
        if (fieldType == String.class) {
            value = CommonUtil.nullToEmpty(value);
        }
        if (fieldType == Integer.class) {
            value = Integer.parseInt(CommonUtil.nullToEmpty(value));
        }
        if (fieldType == Long.class) {
            value = Long.parseLong(CommonUtil.nullToEmpty(value));
        }
        if (fieldType == Double.class) {
            value = Double.parseDouble(CommonUtil.nullToEmpty(value));
        }
        if (fieldType == ObjectId.class) {
            if (!CommonUtil.isObjectId(CommonUtil.nullToEmpty(value))) {
                UpdateInterruptException.throwEx(2, "字段值非ObjectId类型");
            }

            value = new ObjectId(CommonUtil.nullToEmpty(value));
        }
        return value;
    }

    private Long parseDate(String orderDate) {
        try {
            return dateUtils.getDateYYMMDD(orderDate);
        } catch (Exception e) {
        }
        return null;
    }

    private void regexQueryField(Query query, String queryField, Boolean isRegQuery, String fieldName) {
        if (CommonUtil.isStringNotNull(queryField)) {
            if (isRegQuery) {
                query.addCriteria(Criteria.where(fieldName).regex(queryField));
            } else {
                query.addCriteria(Criteria.where(fieldName).is(queryField));
            }
        }
    }
}
