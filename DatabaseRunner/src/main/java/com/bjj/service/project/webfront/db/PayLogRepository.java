/**
 *
 */
package com.bjj.service.project.webfront.db;

import com.bjj.service.model.webfront.PayLog;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author June
 *         CreateTime：2015-3-27  下午2:20:09
 */
public interface PayLogRepository extends MongoRepository<PayLog, ObjectId> {

    @Override
    public <S extends PayLog> S save(S arg0);
}
