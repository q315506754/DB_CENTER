package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.domain.Msg;
import com.bjj.service.exception.MsgException;
import com.bjj.service.model.orderprocess.Admin;
import com.bjj.service.project.orderprocess.inf.IAdminService;
import com.bjj.service.utils.CommonUtil;
import com.bjj.tools.LogUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/2 0002 14:17
 */
@Component
@RMIServiceExporterAnno(serviceInterface = IAdminService.class)
public class AdminImpl implements IAdminService {
    private LogUtil log = LogUtil.getInstance("orderProcess");

    @Autowired
    private MongoOperations mongoOperationNew;

    @Override
    public JSONObject doLogin(JSONObject param) {
        Msg<JSONObject> ret = new Msg<JSONObject>();
        try {
            Object o_user = param.get("user");
            Object o_pwd = param.get("pwd");
            ret.throwIfNullException(o_user, 2, "没有传入账号");
            ret.throwIfNullException(o_pwd, 3, "没有传入密码");
            String user = (String) param.get("user");
            String pwd = (String) param.get("pwd");

            Query query = new Query();
            query.addCriteria(Criteria.where("user").is(user));

            Admin one = mongoOperationNew.findOne(query, Admin.class);
            ret.throwIfNullException(one, 4, "没有该用户");

            if(one.getPwd().length() != 32){
                ret.throwException(7, "您的密码没有加密,请联系管理员为您配置加密密码");
            }
            if (!one.getPwd().equalsIgnoreCase(pwd)) {
                ret.throwException(5, "密码不正确");
            }

            if (one.getIsEnable() != null && !one.getIsEnable()) {
                ret.throwException(6, "账户已被禁用");
            }

            JSONObject data = JSONObject.fromObject(one);
            data.put("id", one.getId().toString());
            ret.setData(data);

        } catch (MsgException e) {

        } catch (Exception e) {
            log.logError(e.getMessage(), e);
            ret.error(1, "未知异常", e.getClass() + "\t" + e.getMessage());
        }

        JSONObject tRet = new JSONObject();
        tRet.put("code", ret.getCode());
        tRet.put("msg", ret.getMsg());
        tRet.put("data", ret.getData());
        return tRet;
    }

    @Override
    public JSONObject save(JSONObject param) {
        Msg<JSONObject> ret = new Msg<JSONObject>();
        try {
            Object o_user = param.get("user");
            Object o_pwd = param.get("pwd");
            String mobile = CommonUtil.nullToEmpty(param.get("mobile"));
            ret.throwIfNullException(o_user, 2, "没有传入账号");
            ret.throwIfNullException(o_pwd, 3, "没有传入密码");
            String user = (String) param.get("user");
            String pwd = (String) param.get("pwd");

            Query query = new Query();
            query.addCriteria(Criteria.where("user").is(user));

            Admin one = mongoOperationNew.findOne(query, Admin.class);

            if (one != null) {
                ret.throwException(4, "用户名已存在");
            }

            one = new Admin();
            one.setUser(user);
            one.setPwd(pwd);
            one.setIsEnable(true);
            one.setMobile(mobile);
            mongoOperationNew.save(one);

            JSONObject data = JSONObject.fromObject(one);
            data.put("id", one.getId().toString());
            ret.setData(data);
        } catch (MsgException e) {

        } catch (Exception e) {
            log.logError(e.getMessage(), e);
            ret.error(1, "未知异常", e.getClass() + "\t" + e.getMessage());
        }

        JSONObject tRet = new JSONObject();
        tRet.put("code", ret.getCode());
        tRet.put("msg", ret.getMsg());
        tRet.put("data", ret.getData());
        return tRet;
    }
}
