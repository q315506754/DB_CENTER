package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.OrderStatus;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:56
 */
public abstract class StatusChangeDetail implements IStatusChangeDetail {
    private final OrderStatus oldVal;
    private final OrderStatus newValue;

    public StatusChangeDetail(OrderStatus oldVal, OrderStatus newValue) {
        this.oldVal = oldVal;
        this.newValue = newValue;
    }

    public OrderStatus getOldVal() {
        return oldVal;
    }

    public OrderStatus getNewValue() {
        return newValue;
    }
}
