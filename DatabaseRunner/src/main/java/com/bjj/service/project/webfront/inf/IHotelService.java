package com.bjj.service.project.webfront.inf;


import com.bjj.service.project.webfront.domain.Hotel;

import java.util.List;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/5 0005 11:06
 */
public interface IHotelService {
    List<Hotel> searchHotelsByAreaIdAndName(String name);
}
