package com.bjj.service.project.webfront.domain;

import java.io.Serializable;

public class City implements Serializable {
    private String id;
    private String text;
    private String provinceId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    @Override
    public String toString() {
        return "City{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", provinceId='" + provinceId + '\'' +
                '}';
    }
}
