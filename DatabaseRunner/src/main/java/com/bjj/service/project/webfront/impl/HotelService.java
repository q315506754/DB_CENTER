package com.bjj.service.project.webfront.impl;

import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.project.webfront.db.HotelRepository;
import com.bjj.service.project.webfront.domain.Hotel;
import com.bjj.service.project.webfront.inf.IHotelService;
import com.bjj.service.utils.BeanCopyUtil;
import com.bjj.tools.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RMIServiceExporterAnno(serviceInterface = IHotelService.class)
public class HotelService implements IHotelService {
    private LogUtil log = LogUtil.getInstance("webFront");

    @Autowired
    HotelRepository repo;

    @Override
    public List<Hotel> searchHotelsByAreaIdAndName(String name) {
        Pageable page = new PageRequest(0, 10); //限制每次返回最多10条记录
        return BeanCopyUtil.getCopyBeansFromObjecIdToStr(repo.findHotelsByName(name, page), Hotel.class);
    }
}
