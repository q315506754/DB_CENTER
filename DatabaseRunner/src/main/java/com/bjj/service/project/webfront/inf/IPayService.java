/**
 *
 */
package com.bjj.service.project.webfront.inf;

import net.sf.json.JSONObject;

/**
 * @author June
 *         CreateTime：2015-3-27  下午2:38:37
 */
public interface IPayService {

    void savePayLog(JSONObject message);
}
