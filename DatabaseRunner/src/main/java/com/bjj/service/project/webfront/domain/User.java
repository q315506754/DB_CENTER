package com.bjj.service.project.webfront.domain;

import java.io.Serializable;

public class User implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5297968621161872704L;
    private String id;
    private String userName;
    private String passWord;
    private String mobile;
    private String inviteCode;
    private String code;
    private String email;
    private String name;

    //修改密码时用到
    private String newPassword;
    private String type;//标记是否记住密码

    /**
     * 无参构造，当请求为User对象时，SPRING需要
     */
    public User() {
        super();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public String getInviteCode() {
        return inviteCode;
    }


    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }


    public String getCode() {
        return code;
    }


    public void setCode(String code) {
        this.code = code;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getNewPassword() {
        return newPassword;
    }


    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", passWord=" + passWord + ", mobile=" + mobile + ", inviteCode=" + inviteCode + ", code=" + code + ", email=" + email + ", name=" + name
				+ ", newPassword=" + newPassword + ", type=" + type + "]";
	}
}
