/**
 *
 */
package com.bjj.service.project.webfront.impl;

import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.model.webfront.PayLog;
import com.bjj.service.project.webfront.db.PayLogRepository;
import com.bjj.service.project.webfront.inf.IPayService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author June
 *         CreateTime：2015-3-27  下午2:40:21
 */
@Component
@RMIServiceExporterAnno(serviceInterface = IPayService.class)
public class PayService implements IPayService {

    @Autowired
    PayLogRepository pay;

    /* (non-Javadoc)
     * @see com.bjj.service.project.webfront.inf.IPayService#savePayLog(net.sf.json.JSONObject)
     */
    @Override
    public void savePayLog(JSONObject message) {
        PayLog log = new PayLog();
        if (message.containsKey("adminId")) {
            log.setAdminId(message.getString("adminId"));
        }
        if (message.containsKey("form")) {
            log.setForm(message.getJSONObject("form"));
        }
        if (message.containsKey("logType")) {
            log.setLogType(message.getString("logType"));
        }
        if (message.containsKey("orderId")) {
            log.setOrderId(message.getString("orderId"));
        }
        if (message.containsKey("payWay")) {
            log.setPayWay(message.getString("payWay"));
        }
        if (message.containsKey("result")) {
            log.setResult(message.getJSONObject("result"));
        }
        if (message.containsKey("return_url")) {
            log.setReturn_url(message.getString("return_url"));
        }
        if (message.containsKey("source")) {
            log.setSource(message.getString("source"));
        }
        log.setCreateTime(System.currentTimeMillis());
        pay.save(log);
    }

}
