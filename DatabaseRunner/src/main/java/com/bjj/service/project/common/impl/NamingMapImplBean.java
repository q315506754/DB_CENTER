package com.bjj.service.project.common.impl;

import com.bjj.service.project.common.inf.INamingMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanCurrentlyInCreationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.remoting.rmi.RmiServiceExporter;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jiangli
 *         <p/>
 *         CreateTime 2013-8-7 下午4:13:22
 */
//@Component("rmi_bean_naming")
public class NamingMapImplBean implements INamingMap, ApplicationContextAware {

    private static final Log logger = LogFactory.getLog("Runner");
    private final static Map<String, String> map = new HashMap<String, String>();

    static {
        //init();
    }

    private ApplicationContext applicationContext;

    private void init() {
        try {
//            ApplicationContext applicationContext = SpringUtil.getApplicationContext();
            String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
            Field serviceName = RmiServiceExporter.class.getDeclaredField("serviceName");
            serviceName.setAccessible(true);
            //System.out.println("[" + DateUtil.getCurrentDate_YMDHms() + "] Init RMI Services Start ...");
            logger.info("\tInitializing Naming Services  Start ...");
            int i = 1;
            for (String bean : beanDefinitionNames) {
                try {
                    if (bean.equals("namingServiceExporter") || bean.equals("namingService")) {
                        continue;
                    }
                    Object tbean = applicationContext.getBean(bean);
                    if (tbean instanceof RmiServiceExporter) {
                        RmiServiceExporter rmiBean = (RmiServiceExporter) tbean;
                        String serverName = serviceName.get(tbean).toString();
                        String cls = rmiBean.getServiceInterface().getCanonicalName();
                        map.put(serverName, cls);
                        //System.out.println(i + ". " + serverName + "==>" + cls);
                        logger.info("\t\t" + i + ". " + serverName + "==>" + cls);
                        i++;
                    }
                } catch (Exception e) {
                    logger.error("Naming Services Init bean error:" + bean, e);
                    e.printStackTrace();
                }
            }

            logger.info("\t" + "The total count of registered services is " + (i - 1));
        } catch (BeanCurrentlyInCreationException e) {
            logger.error("Naming Services Init BeanCurrentlyInCreationException ===> ", e);
        } catch (BeanCreationException e) {
            logger.error("Naming Services Init BeanCreationException ===> ", e);
        } catch (Exception e) {
            logger.error("Naming Services Init Exception ===> ", e);
        }

        //System.out.println("[" + DateUtil.getCurrentDate_YMDHms() + "] Init RMI Services End ...");

        logger.info("\tInitializing Naming Services  End ...");
    }

    @Override
    public Map<String, String> getNamingMap() {
        return map;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
