package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.model.webfront.ReqOrder;
import com.bjj.service.model.webfront.ReqOrderLog;
import com.bjj.service.utils.CommonUtil;
import net.sf.json.JSONObject;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:29
 */
@Component
public class ReqOrderAspectRemark extends ReqOrderAspect {
    public ReqOrderAspectRemark() {
        super("remark");
    }

    @Override
    public JSONObject beforeChange(Object oldVal, Object newValue, ReqOrder one, Update update, JSONObject upparams) throws OrderPImpl.UpdateInterruptException {
        JSONObject ret = new JSONObject();
        String operAdminName = CommonUtil.nullToEmpty(upparams.get("operAdminName"));
        ret.put("lastOperRemarkAdminName", operAdminName);
        return ret;
    }

    @Override
    public void modifyOrderLog(ReqOrderLog log) {
        log.setOperType(getPropName());
    }
}
