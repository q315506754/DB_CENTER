package com.bjj.service.project.webfront.domain;

import java.io.Serializable;

public class Hotel implements Serializable {
    private String id;
    private String text;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
