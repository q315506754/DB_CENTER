package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.PropChangeAspect;
import com.bjj.service.model.webfront.ReqOrder;
import com.bjj.service.model.webfront.ReqOrderLog;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:23
 */
public abstract class ReqOrderAspect extends PropChangeAspect<ReqOrder> {

    public ReqOrderAspect(String propName) {
        super(propName);
    }

    public  void modifyOrderLog(ReqOrderLog log){

    }
}
