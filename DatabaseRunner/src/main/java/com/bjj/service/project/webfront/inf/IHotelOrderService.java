package com.bjj.service.project.webfront.inf;


import com.bjj.service.domain.Msg;
import com.bjj.service.project.webfront.domain.ChangeExtraService;
import com.bjj.service.project.webfront.domain.ReqOrder;
import com.bjj.service.project.webfront.domain.UpdOrderStatus;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/6 0006 14:27
 */
public interface IHotelOrderService {
    Msg<ReqOrder> saveReqOrder(ReqOrder rOrder);
    
    Msg<UpdOrderStatus> updOrderStatus(UpdOrderStatus updOrderStatus);
    
    void changeExtraService(ChangeExtraService changeExtraService);
}
