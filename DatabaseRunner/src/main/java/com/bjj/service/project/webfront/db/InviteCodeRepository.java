/**
 *
 */
package com.bjj.service.project.webfront.db;

import com.bjj.service.model.webfront.InviteCode;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * @author June
 *         CreateTime：2015-3-23  下午5:44:05
 */
public interface InviteCodeRepository extends MongoRepository<InviteCode, ObjectId> {

    @Override
    public <S extends InviteCode> S save(S arg0);

    @Query("{'mobile':?0,'inviteCode':?1}")
    public InviteCode findNum(String mobile, String inviteCode);

    @Query(value = "{'mobile':?0}")
    public InviteCode findNum(String mobile, Sort sort);

    @Query(value = "{'inviteCode':?0}")
    public InviteCode getObj(String inviteCode);
}
