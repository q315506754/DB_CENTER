package com.bjj.service.project.webfront.domain;

import java.io.Serializable;

public class UpdOrderStatus implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5297968621161872704L;
    private String orderId;
    private String status;
    private String userId;
    private String mobile;
    private String propName;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPropName() {
		return propName;
	}
	public void setPropName(String propName) {
		this.propName = propName;
	}
	
    
    
}
