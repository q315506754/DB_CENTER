package com.bjj.service.project.common.inf;

import java.util.Map;

/**
 * @author Jiangli
 *         <p/>
 *         CreateTime 2013-8-7 下午4:13:44
 */
public interface INamingMap {
    Map<String, String> getNamingMap();
}
