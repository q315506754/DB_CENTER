package com.bjj.service.project.webfront.domain;

import java.io.Serializable;

public class ReqOrder implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5297968621161872704L;
    private String id;
    private Long startDate;
    private Long endDate;
    private Long createTime;
    private String hotelName;
    private String hotelId;
    private String userId;
    private String customName;
    private String bedType;
    private String contactEmail;
    private String contactMobile;
    private String contactName;
    private String roomTypeName;
    private Integer roomNumber;
    private String liveName;
    private Integer liveNumber;

    @Override
    public String toString() {
        return "ReqOrder{" +
                "id='" + id + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", createTime=" + createTime +
                ", hotelName='" + hotelName + '\'' +
                ", hotelId='" + hotelId + '\'' +
                ", userId='" + userId + '\'' +
                ", customName='" + customName + '\'' +
                ", roomNumber='" + roomNumber + '\'' +
                ", bedType='" + bedType + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", contactMobile='" + contactMobile + '\'' +
                ", contactName='" + contactName + '\'' +
                ", roomTypeName='" + roomTypeName + '\'' +
                ", liveName='" + liveName + '\'' +
                ", liveNumber='" + liveNumber + '\'' +
                '}';
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getBedType() {
        return bedType;
    }

    public void setBedType(String bedType) {
        this.bedType = bedType;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}

	public String getLiveName() {
		return liveName;
	}

	public void setLiveName(String liveName) {
		this.liveName = liveName;
	}

	public Integer getLiveNumber() {
		return liveNumber;
	}

	public void setLiveNumber(Integer liveNumber) {
		this.liveNumber = liveNumber;
	}

	
    
}
