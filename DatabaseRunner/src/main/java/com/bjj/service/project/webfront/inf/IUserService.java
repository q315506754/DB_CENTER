package com.bjj.service.project.webfront.inf;


import java.util.List;

import com.bjj.service.exception.ServiceException;
import com.bjj.service.project.webfront.domain.InviteInfo;
import com.bjj.service.project.webfront.domain.User;


public interface IUserService {

    /**
     * 登录判断
     *
     * @param userName
     * @param passWord
     * @return
     * @author June
     * 2015-3-23  下午5:46:05
     */
    User doLogin(String userName, String passWord);

    User getUserMobile(String userName);

    List<User> getAllUsers();

    /**
     * 验证验证码
     *
     * @param mobile
     * @param code
     * @throws ServiceException
     * @author June
     * 2015-3-23  下午5:45:52
     */
    void valiCode(String mobile, String code) throws ServiceException;

    /**
     * 保存验证码
     *
     * @param code
     * @param mobile
     * @author June
     * 2015-3-23  下午5:45:28
     */
    void saveSmsMessage(String code, String mobile);

    /**
     * 保存邀请码
     *
     * @param mobile
     * @param inviteCode
     * @author June
     * 2015-3-23  下午5:55:43
     */
    void saveInviteCode(String mobile, String inviteCode);

    void useInviteCode(String mobile, String inviteCode) throws ServiceException;

    /**
     * 注册新用户
     *
     * @param mobile
     * @param email
     * @param password
     * @author June
     * 2015-3-23  下午5:57:01
     */
    User saveUser(String mobile, String email, String password) throws ServiceException;

    /**
     * 修改密码
     *
     * @param mobile
     * @param password
     * @author June
     * 2015-3-24  上午10:52:19
     */
    void updatePwd(String mobile, String password);

    InviteInfo getNum(String mobile);
    
    boolean verityLogin(String adminId);
}
