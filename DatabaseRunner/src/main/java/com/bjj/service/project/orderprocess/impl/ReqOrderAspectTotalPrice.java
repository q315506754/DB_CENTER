package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.model.webfront.ReqOrder;
import net.sf.json.JSONObject;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:29
 */
@Component
public class ReqOrderAspectTotalPrice extends ReqOrderAspect {

    public ReqOrderAspectTotalPrice() {
        super("totalPrice");
    }

    @Override
    public JSONObject beforeChange(Object oldVal, Object newValue, ReqOrder one, Update update, JSONObject upparams) throws OrderPImpl.UpdateInterruptException {
        throw new OrderPImpl.UpdateInterruptException(200000, "该字段不能直接更新");
    }
}
