package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.model.webfront.ReqOrder;
import org.springframework.data.mongodb.core.query.Update;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:54
 */
public interface IStatusChangeDetail {
    public void beforeChange(ReqOrder one, Update update) throws OrderPImpl.UpdateInterruptException;
}
