package com.bjj.service.model.orderprocess;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/2 0002 14:07
 * @Remark("订单操作后台登陆表")
 */
@Document(collection = "admin")
public class Admin {
    @Id
    private ObjectId id;

    private String user;//账号，唯一
    private String pwd;//登陆密码
    private String mobile;//手机号
    private Boolean isEnable;//是否启用

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Boolean getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
