package com.bjj.service.model;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/27 0027 11:02
 * @Remark("订单生成表,ReqOrder的orderId均由该表事先生成数据")
 */
@Document(collection = "OrderIDS")
public class OrderIDS implements Serializable {
    private static final long serialVersionUID = 1L;

    private ObjectId id;
    private Integer oId;//订单号

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getoId() {
        return oId;
    }

    public void setoId(Integer oId) {
        this.oId = oId;
    }
}
