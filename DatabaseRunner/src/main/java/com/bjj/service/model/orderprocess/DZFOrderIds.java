package com.bjj.service.model.orderprocess;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/2 0002 14:07
 * @Remark("订单待支付记录表,用于超时取消")
 */
@Document(collection = "DZFOrderIds")
public class DZFOrderIds {
    @Id
    private ObjectId id;

    private String orderId;//ReqOrder.orderId
    private Boolean u;//是否已被处理
    private Long time;//变成待支付的时间

    @Override
    public String toString() {
        return "DZFOrderIds{" +
                "id=" + id +
                ", orderId='" + orderId + '\'' +
                ", u=" + u +
                ", time=" + time +
                '}';
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Boolean getU() {
        return u;
    }

    public void setU(Boolean u) {
        this.u = u;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
