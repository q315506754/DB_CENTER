package com.bjj.service.model.webfront;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/27 0027 11:02
 * @Remark("订单表")
 */
@Document(collection = "reqorder")
public class ReqOrder implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5297968621161872704L;
    @Id
    private ObjectId id;
    private Long startDate;//入住日期
    private Long endDate;//离店日期
    private Long createTime;//订单创建日期
    private String hotelName;//入住酒店
    private ObjectId hotelId;//入住酒店id，对应Hotel._id
    private ObjectId userId;//下单人id
    private ObjectId providerId;//供应商id，对应Provider._id
    private Integer roomNumber;//房间数量
    //    private Integer roomType;//房型代码
    private String roomTypeName;//房型名称
    private String broadband;//宽带
    private String cancelRule;//取消规则
    private Integer breakfastNum;//早餐数
    //    private Integer bedType;//床型代码
    private String bedTypeName;//床型名称
    private String orderId;//订单号
    private String status;//状态代码
    private String contactName;//联系人姓名
    private String contactPhone;//联系人手机
    private String contactEmail;//联系人邮箱
    private String liveName;//入住人姓名
    private String livePhone;//入住人手机
    private String liveEmail;//入住人邮箱
    private Integer liveNumber;//入住人数
    private String providerOrderNum;//供应商订单号
    private String okNum;//确认号
    private String remark;//跟单备注
    private Double totalPrice;//订单总价，为unit的所有totalPrice的和
    private List<UnitPrice> unit;//订单各天价格
    private List<ExtraService> extraService;//额外增值服务

    public List<ExtraService> getExtraService() {
        return extraService;
    }

    public void setExtraService(List<ExtraService> extraService) {
        this.extraService = extraService;
    }

    public ObjectId getProviderId() {
        return providerId;
    }

    public void setProviderId(ObjectId providerId) {
        this.providerId = providerId;
    }

    public String getBroadband() {
        return broadband;
    }

    public String getCancelRule() {
        return cancelRule;
    }

    public void setCancelRule(String cancelRule) {
        this.cancelRule = cancelRule;
    }

    public void setBroadband(String broadband) {
        this.broadband = broadband;
    }

    public Integer getBreakfastNum() {
        return breakfastNum;
    }

    public void setBreakfastNum(Integer breakfastNum) {
        this.breakfastNum = breakfastNum;
    }

    public String getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName;
    }

    public String getBedTypeName() {
        return bedTypeName;
    }

    public void setBedTypeName(String bedTypeName) {
        this.bedTypeName = bedTypeName;
    }

    public List<UnitPrice> getUnit() {
        return unit;
    }

    public void setUnit(List<UnitPrice> unit) {
        this.unit = unit;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getLiveEmail() {
        return liveEmail;
    }

    public void setLiveEmail(String liveEmail) {
        this.liveEmail = liveEmail;
    }

    public String getProviderOrderNum() {
        return providerOrderNum;
    }

    public void setProviderOrderNum(String providerOrderNum) {
        this.providerOrderNum = providerOrderNum;
    }

    public String getOkNum() {
        return okNum;
    }

    public void setOkNum(String okNum) {
        this.okNum = okNum;
    }


    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getLiveName() {
        return liveName;
    }

    public void setLiveName(String liveName) {
        this.liveName = liveName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }


    public String getLivePhone() {
        return livePhone;
    }

    public void setLivePhone(String livePhone) {
        this.livePhone = livePhone;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    /**
     * 无参构造，当请求为User对象时，SPRING需要
     */
    public ReqOrder() {
        super();
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public ObjectId getHotelId() {
        return hotelId;
    }

    public void setHotelId(ObjectId hotelId) {
        this.hotelId = hotelId;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }


    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }


    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public Integer getLiveNumber() {
        return liveNumber;
    }

    public void setLiveNumber(Integer liveNumber) {
        this.liveNumber = liveNumber;
    }

    @Override
    public String toString() {
        return "ReqOrder{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", createTime=" + createTime +
                ", hotelName='" + hotelName + '\'' +
                ", hotelId=" + hotelId +
                ", userId=" + userId +
                ", providerId=" + providerId +
                ", roomNumber=" + roomNumber +
                ", roomTypeName='" + roomTypeName + '\'' +
                ", broadband='" + broadband + '\'' +
                ", cancelRule='" + cancelRule + '\'' +
                ", breakfastNum=" + breakfastNum +
                ", bedTypeName='" + bedTypeName + '\'' +
                ", orderId='" + orderId + '\'' +
                ", status='" + status + '\'' +
                ", contactName='" + contactName + '\'' +
                ", contactPhone='" + contactPhone + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", liveName='" + liveName + '\'' +
                ", livePhone='" + livePhone + '\'' +
                ", liveEmail='" + liveEmail + '\'' +
                ", providerOrderNum='" + providerOrderNum + '\'' +
                ", okNum='" + okNum + '\'' +
                ", remark='" + remark + '\'' +
                ", totalPrice=" + totalPrice +
                ", unit=" + unit +
                '}';
    }
}
