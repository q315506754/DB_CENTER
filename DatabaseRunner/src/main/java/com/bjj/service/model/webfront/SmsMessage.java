/**
 *
 */
package com.bjj.service.model.webfront;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author June
 *         CreateTime：2015-3-23  下午3:39:35
 */
@Document(collection = "SmsMessage")
public class SmsMessage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    private ObjectId id;
    private String mobile;
    private String code;// 验证码
    private long validate;// 有效期

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getValidate() {
        return validate;
    }

    public void setValidate(long validate) {
        this.validate = validate;
    }

    /**
     * @param id
     * @param mobile
     * @param code
     * @param validate
     */
    @PersistenceConstructor
    public SmsMessage(ObjectId id, String mobile, String code, long validate) {
        super();
        this.id = id;
        this.mobile = mobile;
        this.code = code;
        this.validate = validate;
    }

    /**
     *
     */
    public SmsMessage() {
        super();
    }

    @Override
    public String toString() {
        return "SmsMessage [id=" + id + ", mobile=" + mobile + ", code=" + code + ", validate=" + validate + "]";
    }

}
