package com.bjj.service.model.webfront;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/27 0027 15:00
 * @Remark("供应商表")
 */
@Document(collection = "provider")
public class Provider {
    @Id
    private ObjectId id;
    private String name;//供应商名称
    private String tel;//供应商座机
    private String email;//供应商邮箱
    private String contactName;//供应商联系人姓名
    private String contactPhone;//供应商联系人手机

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }
}
