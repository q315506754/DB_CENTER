package com.bjj.service.model.webfront;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/27 0027 11:02
 * @Remark("订单日志表")
 */
@Document(collection = "reqorderlog")
public class ReqOrderLog {
    @Id
    private ObjectId id;
    private String orderId;//订单号
    private Long operTime;//操作时间
    private String content;//操作内容
    private String operAdminName;//操作内容
    private String operAdminMobile;//操作内容
    //    private ObjectId operAdminId;//操作人id，根据operAdminType字段判断属于哪个表
    private String operIp;//操作人ip
//    private Integer operAdminType;//0:系统/default 1:后台 2:前端 决定operAdminId所在表
    private String operType;//remark

    public String getOperAdminName() {
        return operAdminName;
    }

    public void setOperAdminName(String operAdminName) {
        this.operAdminName = operAdminName;
    }

    public String getOperAdminMobile() {
        return operAdminMobile;
    }

    public void setOperAdminMobile(String operAdminMobile) {
        this.operAdminMobile = operAdminMobile;
    }

    public String getOperIp() {
        return operIp;
    }

    public void setOperIp(String operIp) {
        this.operIp = operIp;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getOperTime() {
        return operTime;
    }

    public void setOperTime(Long operTime) {
        this.operTime = operTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ReqOrderLog{" +
                "id=" + id +
                ", orderId='" + orderId + '\'' +
                ", operTime=" + operTime +
                ", content='" + content + '\'' +
                ", operAdminName='" + operAdminName + '\'' +
                ", operAdminMobile='" + operAdminMobile + '\'' +
                ", operIp='" + operIp + '\'' +
                ", operType='" + operType + '\'' +
                '}';
    }

    public String getOperType() {
        return operType;
    }

    public void setOperType(String operType) {
        this.operType = operType;
    }
}
