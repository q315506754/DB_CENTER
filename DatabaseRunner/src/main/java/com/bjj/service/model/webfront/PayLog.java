/**
 * 
 */
package com.bjj.service.model.webfront;

import java.io.Serializable;

import net.sf.json.JSONObject;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author June
 * CreateTime：2015-3-27  下午2:21:07
 */
@Document(collection="PayLog")
public class PayLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private ObjectId id;
	private String adminId;// 登录人
	private JSONObject form;// 请求参数
	private String logType;
	private String orderId;
	private String source;
	private String return_url;
	private JSONObject result;// 回调结果
	private String payWay;
	private Long createTime;// 创建时间

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public JSONObject getForm() {
		return form;
	}

	public void setForm(JSONObject form) {
		this.form = form;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	public JSONObject getResult() {
		return result;
	}

	public void setResult(JSONObject result) {
		this.result = result;
	}

	public String getPayWay() {
		return payWay;
	}

	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "PayLog [id=" + id + ", adminId=" + adminId + ", form=" + form + ", logType=" + logType + ", orderId=" + orderId + ", source=" + source + ", return_url=" + return_url + ", result="
				+ result + ", payWay=" + payWay + ", createTime=" + createTime + "]";
	}

}
