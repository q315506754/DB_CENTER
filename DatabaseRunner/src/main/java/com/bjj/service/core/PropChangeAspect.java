package com.bjj.service.core;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:13
 */
public abstract class PropChangeAspect<C> implements PropChangeInterface<C> {
    private final String propName;

    public PropChangeAspect(String propName) {
        this.propName = propName;
    }

    public String getPropName() {
        return propName;
    }
}
