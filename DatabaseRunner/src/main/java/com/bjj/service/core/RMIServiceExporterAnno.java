package com.bjj.service.core;

import java.lang.annotation.*;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/5 0005 10:29
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RMIServiceExporterAnno {
    Class<?> serviceInterface();
}
