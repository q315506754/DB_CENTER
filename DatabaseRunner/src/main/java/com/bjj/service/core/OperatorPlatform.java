package com.bjj.service.core;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/1 0001 15:49
 */
public enum OperatorPlatform {
    System(0, "系统"), OrderProcess(1, "订单操作后台"), WebFront(2, "客户前端"), Unkown(-999, "未知平台");


    OperatorPlatform(int code, String platformName) {
        this.code = code;
        this.platformName = platformName;
    }

    private int code;
    private String platformName;

    public static OperatorPlatform get(Integer code) {
        if (code == null) {
            return System;
        }
        for (OperatorPlatform orderStatus : values()) {
            if (orderStatus.getCode() == code.intValue()) {
                return orderStatus;
            }
        }
        return Unkown;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }
}
