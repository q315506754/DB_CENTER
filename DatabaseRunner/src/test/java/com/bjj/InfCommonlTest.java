package com.bjj;

import com.bjj.service.core.OrderIdGen;
import com.bjj.service.project.webfront.domain.User;
import com.bjj.service.project.webfront.inf.IUserService;
import com.bjj.service.utils.DateUtils;
import com.bjj.service.utils.SpringUtil;
import com.bjj.tools.PropertiesUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class InfCommonlTest {
    @Autowired
    private IUserService iUserService;


    @Autowired
    private OrderIdGen orderIdGen;

    @Autowired
    private DateUtils dateUtils;

    @Test
    public void func() {
        System.out.println(iUserService);
        User bjj = iUserService.doLogin("bjj", "123123");
        System.out.println(bjj);
    }

    @Test
    public void funcG1() {
        System.out.println(orderIdGen);
        System.out.println(orderIdGen.generateNewOrderId());
        System.out.println(orderIdGen.generateNewOrderId());
        System.out.println(orderIdGen.generateNewOrderId());
        System.out.println(orderIdGen.generateNewOrderId());
    }

    @Test
    public void funcD1() {
        Date date = new Date();
        System.out.println(date);
        System.out.println(dateUtils.getNextDate(date));
        System.out.println(date);
    }

    @Test
    public void func2() {
        System.out.println(iUserService);
        User bjj = iUserService.doLogin("bjj", "123123");
        System.out.println(bjj);
        System.out.println(SpringUtil.getApplicationContext());
        System.out.println(PropertiesUtil.readStringValue("sdsdds"));
        System.out.println(PropertiesUtil.readStringValue("rmi.service.prefix"));
    }
    @Test
    public void func3() {
        System.out.println(iUserService);
        User bjj = iUserService.doLogin("bjj", "xxxd");
        System.out.println(bjj);
        System.out.println(SpringUtil.getApplicationContext());
        System.out.println(PropertiesUtil.readStringValue("sdsdds"));
    }
}
