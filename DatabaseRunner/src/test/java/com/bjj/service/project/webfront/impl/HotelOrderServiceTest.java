package com.bjj.service.project.webfront.impl;

import com.bjj.service.core.OrderIdGen;
import com.bjj.service.core.OrderStatus;
import com.bjj.service.domain.Msg;
import com.bjj.service.model.webfront.Provider;
import com.bjj.service.model.webfront.User;
import com.bjj.service.project.webfront.db.HotelOrderRepository;
import com.bjj.service.project.webfront.domain.ReqOrder;
import com.bjj.service.project.webfront.domain.ReqOrder_Contact;
import com.bjj.service.utils.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Random;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class HotelOrderServiceTest {

    @Autowired
    HotelOrderRepository query;
    @Autowired
    private HotelOrderService hotelOrderService;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private MongoOperations mongoOperationNew;

    @Test
    @Repeat(16)
    public void testSaveReqOrder() throws Exception {
        System.out.println(hotelOrderService);

        ReqOrder param = new ReqOrder();
        param.setCustomName("自定义酒店");
        param.setStartDate(dateUtils.getDateYYMMDD("2015-04-01"));
        param.setEndDate(dateUtils.getDateYYMMDD("2015-04-10"));
        param.setRoomNumber(3);
        param.setBedType("千人床");
//        param.setHotelId("123412341234123412341234");
//        param.setHotelId(null);
        param.setHotelId("4ee1cb3a7c402326b2278c89");
        param.setHotelName("随便打酒店");

        param.setContactEmail("315506754@qq.com");
        param.setContactMobile("13761156786");
        param.setContactName("联系人jiangli222");

        User one = mongoOperationNew.findOne(new Query(), User.class);
        param.setUserId(one.getId().toString());

        Msg<ReqOrder> reqOrderMsg = hotelOrderService.saveReqOrder(param);
        System.out.println(reqOrderMsg);

    }

    @Test
    public void testFindReqOrder() throws Exception {
        List<com.bjj.service.model.webfront.ReqOrder> reqOrders = query.queryAllOrders();
        for (com.bjj.service.model.webfront.ReqOrder reqOrder : reqOrders) {
            System.out.println(reqOrder);
        }
    }
    @Test
    public void testIDG() throws Exception {
        List<com.bjj.service.model.webfront.ReqOrder> reqOrders = query.queryAllOrders();
        for (com.bjj.service.model.webfront.ReqOrder reqOrder : reqOrders) {
            System.out.println(reqOrder);
        }
    }

    @Autowired
    HotelOrderRepository repo;

    @Autowired
    OrderIdGen orderIdGen;


    @Test
    @Repeat(20)
    public void testSaveProvider() throws Exception {
        Provider o = new Provider();
        Random r = new Random();
        o.setName("深圳捷旅供应商"+r.nextInt(100000));
        o.setContactName("联系人故居级"+r.nextInt(100000));
        o.setContactPhone("183"+r.nextInt(100000));
        o.setTel("021-66"+r.nextInt(100000));
        o.setEmail(r.nextInt(100000)+"@gmail.com");
        mongoOperationNew.save(o);
    }

    @Test
    public void testSave() throws Exception {
        com.bjj.service.model.webfront.ReqOrder dbModel = new com.bjj.service.model.webfront.ReqOrder();
        dbModel.setOrderId(orderIdGen.generateNewOrderId());
        dbModel.setContactName("联系人_jljljl");
        dbModel.setContactPhone("联系人手机_1333333");
        dbModel.setLiveName("入住人姓名_j1kjljljl");
        dbModel.setOkNum("确认号_888888");
        dbModel.setProviderOrderNum("供应商订单号_6666666666666");
        dbModel.setCreateTime(System.currentTimeMillis());
        dbModel.setStartDate(dateUtils.getDateYYMMDD("2015-03-01"));
        dbModel.setEndDate(dateUtils.getDateYYMMDD("2015-03-22"));
        dbModel.setHotelName("阿什顿佛撒发生发顺丰大酒店");
        dbModel.setStatus(OrderStatus.XJZ.getEnName());
        dbModel.setRoomNumber(5);
        dbModel.setRemark("备注备注备注备注备注备注备注备注备注备注备注备注备注备注备注备注备注备注备注");
        dbModel.setTotalPrice(88489.12);
        dbModel.setRoomTypeName("超级海景房");
        dbModel.setCancelRule("随时随地取消");
        dbModel.setBreakfastNum(99);
        dbModel.setBedTypeName("10人床");
        dbModel.setBroadband("有1000M宽带");

        Provider one = mongoOperationNew.findOne(new Query(), Provider.class);
        if (one != null) {

        dbModel.setProviderId(one.getId());
        }
//        dbModel.setStatus(OrderStatus.DQR.getEnName());
        repo.save(dbModel);
    }

    @Test
    public void testBeanCopy2() throws Exception {
        List<com.bjj.service.model.webfront.ReqOrder> reqOrders = query.queryAllOrders();
        for (com.bjj.service.model.webfront.ReqOrder reqOrder : reqOrders) {
            System.out.println(reqOrder);

        }
    }

}