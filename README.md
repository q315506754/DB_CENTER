#简介：<br/>
这是数据库项目

#FAQ
#<strong style="font-color:gray"><a name="howToIntegrate">如何与Web项目整合?</a></strong><br/>

1.用build-rmi-jar.xml通过ant来打包生成一个名为service-rmi.jar的jar包，放到相应web工程的lib/下。<br/>
（可以通过修xml内属性<br/>
&nbsp;&nbsp;\<property name="package.file" value="D:\Documents\GitHub\JAVA_WEB\MyFrameWork3\WebContent\WEB-INF\lib\service-rmi.jar"/><br/>
来简化这个步骤）
<br/><br/>
2.在Web项目的Spring配置文件中添加RepositoryBean<br/>
    \<bean id="proxyRepository" class="com.bjj.service.utils.ProxyRepository" init-method="init"><br/>
       &nbsp;&nbsp; &nbsp;&nbsp;\<property name="rmiServerIP">\<value>${rmi.server.ip}\</value>\</property><br/>
       &nbsp;&nbsp; &nbsp;&nbsp;\<property name="rmiServerPort">\<value>${rmi.server.port}\</value>\</property><br/>
    \</bean><br/>
<br/><br/>
3.1.通过注解方式获得rmi实例，一般加在controller内，示例如下：<br/>
    @Autowired(required = false)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;private IArea areaBean;<br/>
<br/>
3.2.(<strong>不推荐此方式&nbsp;</strong>)通过ProxyRepository类实例传入rmi服务名，再通过强转方式获得rmi实例，示例如下:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;IArea areaSrv = (IArea)ProxyRepository.getInstance().getProxy("areaSrv").getObject();<br/>
服务名命名规则详见<a href="#howToGenerateRMISrcName"><strong>RMI服务名的生成方式</strong></a>
<br/>
4.先运行数据库项目(com.bjj.service.run.Runner.main)，再运行Web项目进行测试。
#
#<strong>如何增加一个Web项目的接口?</strong><br/>
1.在src\main\java\com\bjj\service\project\路径下添加一个包,包名推荐与Web项目名相关；<br/>该包下必须的包：inf/,impl/，分别存放接口和实现类;<br/>还可定义其它的包，如：db/，目前用于存放基于Mongo注解的操作类，已被Spring自动扫描。<br/>
(\<mongo:repositories base-package="com.bjj.service.project.*.db")<br/><br/>
2.通过注解方式注册Rmi服务,将impl/下的实现类上加上@RMIServiceExporterAnno注解，示例如下：<br/>
@Component<br/>
@RMIServiceExporterAnno(serviceInterface = IHotelService.class)<br/>
public class HotelService implements IHotelService {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;@Autowired<br/>
&nbsp;&nbsp;&nbsp;&nbsp;HotelRepository repo;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;.....<br/>
其中注解中的参数——serviceInterface指的是将暴露给Web项目调用的接口，且该类必须实现参数接口，否则数据库项目运行时会提示出错。<br/><br/>

3.打包到Web工程即可调用，详见<a href="#howToIntegrate"><strong style="font-color:gray">如何与Web项目整合</strong></a><br/><br/>


#<strong><a name="howToGenerateRMISrcName">RMI服务名的生成方式?</a></strong><br/>
目前是service_exporter_加上服务实现类的bean名。例如：<br/>
@Component<br/>
@RMIServiceExporterAnno(serviceInterface = IArea.class)<br/>
public class TestArea2 implements IArea {<br/>
.....<br/>
其中@Component被处理后会生成一个名为testArea2的bean，位于Spring容器中，所以服务名是service_exporter_testArea2。<br/><br/>

#<strong>一个接口有多个实现类并且都注册成RMI服务可以吗?</strong><br/>
可以。<br/>
例如类TestArea2和AreaService都实现了IArea接口，数据库运行后出现如下打印：<br/>
1. service_exporter_areaService==>com.bjj.service.project.webfront.inf.IArea<br/>
2. service_exporter_hotelService==>com.bjj.service.project.webfront.inf.IHotelService<br/>
3. service_exporter_testArea2==>com.bjj.service.project.webfront.inf.IArea<br/>
4. service_exporter_userService==>com.bjj.service.project.webfront.inf.IUserService<br/>
行1和行3说明两个类都已成功注册了RMI服务。<br/><br/>
<strong>注意!&nbsp;</strong><br/>
这时在Web项目如果使用了注解来获得接口，如下：<br/>
 @Autowired(required = false)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;private IArea areaBean;<br/>
将会出现注入错误：<br/>
org.springframework.beans.factory.NoUniqueBeanDefinitionException: No qualifying bean of type [com.bjj.service.project.webfront.inf.IArea] is defined: expected single matching bean but found 2: rmi_bean_service_exporter_testArea2,rmi_bean_service_exporter_areaService<br/><br/>
这时候指定服务名可以解决错误(服务对应的RMIBean已注册到本地Spring容器里)。<br/>
 @Resource(name = "service_exporter_testArea2")<br/>
private IArea areaBean;<br/>
<br/>
